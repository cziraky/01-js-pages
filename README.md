
## Contact
**Repository Owner**: Balázs Cziráky (balazs@cziraky.io)

**Repository Maintainers**: Balázs Cziráky (balazs@cziraky.io)

---

## Practice Lab for Óbuda University
### DevOps - Scripting Languages & Frameworks

### Tasks

1. Fork this base project from https://gitlab.com/0x422e_education/obuda-university/devops/scripting-frameworks/01-js-pages 


2. Continue reading this README 


3. Deploy the web app in the /ui directory to GitLab Pages 

    See: https://docs.gitlab.com/ee/user/project/pages/getting_started/pages_from_scratch.html#specify-a-stage-to-deploy

    ```yml
    image: alpine:latest

    # Include a K6 performance test pipeline
    # https://gitlab.com/0x422e_education/obuda-university/devops/tools/k6
    
    stages:
      - deploy

    # Deploy the wep app to the GitLab Pages
    pages:
      stage: deploy
      script:
        - echo "Hello NOP..."
      artifacts:
        paths:
          - public
      only:
        - master
    ```

4. Request random strings from https://random.org using the `random.js` and print it to the console (`index.html`)

    See: https://www.random.org/clients/http/api/

    ```javascript
    // Fix the 2 issues in the fetch
    async function getRandomString(number, length, format) {
    let response = await fetch("https://www.random.org/strings/?num=1&len=4&digits=on&upperalpha=on&loweralpha=on&unique=on&format=html&rnd=new");
    return await response.json();
    }
    
    // Call getRandomString() 10 times with increasing by 1 (length++)
    getRandomString(1, 16, "plain").then(data => console.log(data));
    ```


5. Include the K6 load test downstream pipeline in your CI/CD pipeline and get the average duration: 
    ```bash
    iteration_duration.............: avg=36.73ms 
    ```